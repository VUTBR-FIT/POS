#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h> 

#include <stdio.h>
#include <stdlib.h>

/**
 * Napise na STDOUT napovedu
 */
void printHelp() {
    printf("Usage\n");
    printf("fork PROGRAM [PARAM1 [PARAM2 [...]]\n");
}

/**
 * Proces napise na STDOUT informace o sobe 
 * @param label Typ procesu
 */
void printBefore(char *label) {
    printf("%s identification: \n", label); /*grandparent/parent/child */
    printf("	pid = %d,	ppid = %d,	pgrp = %d\n", getpid(), getppid(), getpgrp());
    printf("	uid = %d,	gid = %d\n", (int) getuid(), (int) getgid());
    printf("	euid = %d,	egid = %d\n", (int) geteuid(), (int) getegid());
}

/**
 * Proces napise na STDOUT duvod ukonceni potomka
 * @param label Typ procesu
 * @param status Typ ukonceni
 */
void printAfter(char *label, int status) {
    printf("%s exit (pid = %d):", label, getpid()); /* and one line from */
    if (WIFEXITED(status))
        printf("	normal termination (exit code = %d)\n", WEXITSTATUS(status)); /* or */
    else if (WIFSIGNALED(status))
        printf("	signal termination %s(signal = %d)\n", (WCOREDUMP(status) ? "with core dump " : ""), WTERMSIG(status)); /* or */
    else
        printf("	unknown type of termination\n");
}
/**
 * Funkce reprezentujici proces typu child, ktere predava rizeni programu predaneho jako prvni parametr
 * @param argc Pocet vstupnich parametru
 * @param argv Pole zadanych parametru
 * @return Uspech operace
 */
int child(int argc, char *argv[]) {
    printBefore("child");
    if (execv(argv[1], &argv[2]) < 0)
        return 1;
    return 0;
}
/**
 * Funkce reprezentujici proces typu parent, vytvori proces typu child a ceka na jeho ukonceni
 * @param argc Pocet vstupnich parametru
 * @param argv Pole zadanych parametru
 * @return Uspech operace
 */
int parent(int argc, char *argv[]) {
    printBefore("parent");
    int status;
    pid_t id = fork();
    if (id == 0) {
        child(argc, argv);
    } else {
        if (id != -1) {
            signal(SIGINT, SIG_IGN);
            waitpid(id, &status, 0);
            printAfter("parent", status);
        } else {
            perror("fork fail");
            return 1;
        }
    }
    return 0;
}
/**
 * Funkce reprezentujici proces typu grandparent, vytvori proces typu parent a ceka na jeho ukonceni
 * @param argc Pocet vstupnich parametru
 * @param argv Pole zadanych parametru
 * @return Uspech operace
 */
int grandparent(int argc, char *argv[]) {
    printBefore("grandparent");
    int status;
    pid_t id = fork();
    if (id == 0) {
        parent(argc, argv);
    } else {
        if (id != -1) {
            signal(SIGINT, SIG_IGN);
            waitpid(id, &status, 0);
            printAfter("grandparent", status);
        } else {
            perror("fork fail");
            return 1;
        }
    }
    return 0;
}

int main(int argc, char **argv) {
    if (argc >= 2)
        grandparent(argc, argv);
    else printHelp();
} 