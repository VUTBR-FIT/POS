#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h> 

#include <stdio.h>

int main(int argc, char **argv) {
    if (argc > 1) {
        sleep(10);
        return 0;
    } else {
        return 1;
    }
}