/* 
 * File:   main.c
 * Author: user
 *
 * Created on April 28, 2018, 9:43 AM
 */

#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <ctype.h>

#ifdef DEBUG_ENABLED
#define DEBUG_LOG(...) fprintf(stdout, "[%s]\t %s\n", __VA_ARGS__);
#define DEBUG_PRINT(...) fprintf(stdout, __VA_ARGS__);
#else
#define DEBUG_LOG(...) while (false);
#define DEBUG_PRINT(...) while (false);
#endif

#define BUFFER_SIZE 513

#define REDIRECT_OUT '>'
#define REDIRECT_IN '<'
#define RUN_ON_BACKGROUND '&'

struct input {
    char data[BUFFER_SIZE];
    unsigned int lengt;
    int invalid;
};

struct parsed_input {
    char program[BUFFER_SIZE];
    int length;
    bool inBackgound;
    char out[BUFFER_SIZE];
    int inLen;
    char in[BUFFER_SIZE];
    int outLen;
};

enum program_status {
    STOP = 1,
    CONTINUE,
    READING,
    INVALID_STATE
};
int program;
int programStatus;
int threadStatus;

char buffer[BUFFER_SIZE];
char *pathValue;
struct input inputData;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

void signalHandler(int signal);
void *threadRead();
void *threadRunable();
void printErrorCode(char *location, char *message, int code);
void printError(char *message);
void printPromt(char *command);
char *getEnvVar(const char *name, unsigned int length);
void executeCommand();
void parseInput(struct parsed_input *parsed);
bool isRunOnBackground();
char *getIORedirect(char type);
bool isEmptyCommand(char *data, int length) {
   for (int i = 0; i < length; i++) {
        switch ((int) data[i]) {
            case ' ':
            case '\n':
            case '\r':
            case '\0':
                continue;
            default:
                return false;
        }
    }
    return true;
}
void initForkHandler(int signal, struct sigaction *sig, void (*handler) (int)) {
    // @TODO
    sig->sa_handler = handler;
    sigaction(signal, sig, NULL);
} 
void forkBackground(int signal) {
    int status = 0;
    pid_t temp;
    while ((temp = waitpid(-1, &status, WNOHANG)) > 0) {
        if (signal != SIGCHLD)
            continue;
        // @TODO
        fflush(stdout);
    }
}
void forkInteruptHandler(int signal) {
    if (signal == SIGINT) {
        kill(program, SIGINT);
    }
}
bool checkRedirection(char *name, char type) {
    
}

int main(int argc, char** argv) {
    signal(SIGINT, signalHandler);
    pthread_t recv, runable;
    pthread_attr_t attr;
    DEBUG_LOG("MAIN", "start");
    int res;

    pathValue = getEnvVar("PATH", 4);
    fprintf(stdout, "%s\n", pathValue);

    // Inicializace mutexu
    if ((res = pthread_mutex_init(&mutex, NULL)) != 0) {
        printErrorCode("MAIN", "pthread_mutex_init() fail", res);
        return (EXIT_FAILURE);
    }

    // Vytvoreni atributu
    if ((res = pthread_attr_init(&attr)) != 0) {
        pthread_mutex_destroy(&mutex);
        printErrorCode("MAIN", "pthread_attr_init() fail", res);
        return (EXIT_FAILURE);
    }
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
    if ((res = pthread_create(&recv, &attr, &threadRead, NULL)) != 0) {
        pthread_mutex_destroy(&mutex);
        pthread_attr_destroy(&attr);
        printErrorCode("MAIN", "pthread_create(RECV) fail", res);
        return (EXIT_FAILURE);
    }
    if ((res = pthread_create(&runable, &attr, &threadRunable, NULL)) != 0) {
        pthread_mutex_destroy(&mutex);
        pthread_attr_destroy(&attr);
        printErrorCode("MAIN", "pthread_create(RUNABLE) fail", res);
        return (EXIT_FAILURE);
    }

    programStatus = CONTINUE;
    threadStatus = READING;
    // Nastaveni cekani na ukonceni vlakna
    if ((res = pthread_join(runable, NULL)) != 0) {
        pthread_mutex_destroy(&mutex);
        pthread_attr_destroy(&attr);
        printErrorCode("MAIN", "pthread_join(RUNABLE) fail", res);
        return (EXIT_FAILURE);
    }
    if ((res = pthread_join(recv, NULL)) != 0) {
        pthread_mutex_destroy(&mutex);
        pthread_attr_destroy(&attr);
        printErrorCode("MAIN", "pthread_join(RECV) fail", res);
        return (EXIT_FAILURE);
    }

    DEBUG_LOG("MAIN", "End");
    return (EXIT_SUCCESS);
}

void signalHandler(int signal) {
    programStatus = STOP;

}

void printErrorCode(char *location, char *message, int code) {
    fprintf(stderr, "[%s]\t Message: %s (%d)\n", location, message, code);
}

void printError(char *message) {
    inputData.invalid = true;
    fprintf(stderr, "ERROR: %s\n", message);
}

char *getEnvVar(const char *name, unsigned int length) {
    char *value;
    value = getenv(name);

    if (value) {
        char *temp = (char *) malloc(length * sizeof (char));
        for (int i = 0; i <= length; i++)
            if (i == length)
                temp[i] = '=';
            else temp[i] = name[i];
        char *result = strstr(value, temp);
        free(temp);
        temp = NULL;
        if (result == NULL) return value;
        else return result;
    } else return "";
}

void *threadRead() {
    DEBUG_LOG("THREAD_READ", "IN");
    bool invalidInput;
    while (programStatus == CONTINUE) {
        pthread_mutex_lock(&mutex);

        threadStatus = READING;
        inputData.invalid = false;
        DEBUG_LOG("THREAD_READ", "START");
        while (true) {
            printPromt("$ ");
            memset(inputData.data, 0, BUFFER_SIZE);
            inputData.lengt = 0;
            inputData.lengt = read(STDIN_FILENO, inputData.data, BUFFER_SIZE);
            inputData.invalid = inputData.lengt == BUFFER_SIZE && inputData.data[BUFFER_SIZE - 1] != '\n';

            while (inputData.invalid && (inputData.data[inputData.lengt - 1] != '\n'))
                inputData.lengt = read(STDIN_FILENO, inputData.data, BUFFER_SIZE);

            if (invalidInput) break;

            if (inputData.lengt - 1 > 0 && inputData.data[inputData.lengt - 1] == '\n') {
                inputData.data[inputData.lengt - 1] = '\0';
                break;
            }

        }
        printPromt(inputData.data);
        DEBUG_PRINT("[%s]\t LENGT: %d, Data: %s\n", "THREAD_READ", inputData.lengt, inputData.data);
        DEBUG_LOG("THREAD_READ", "END");
        threadStatus = CONTINUE;

        pthread_cond_signal(&cond);
        pthread_cond_wait(&cond, &mutex);
        pthread_mutex_unlock(&mutex);
    }
}

void printPromt(char* command) {
    fprintf(stdout, "%s", command);
}

void *threadRunable() {
    DEBUG_LOG("THREAD_RUNABLE", "IN");
    while (programStatus == CONTINUE) {
        pthread_mutex_lock(&mutex);
        while (threadStatus == READING)
            pthread_cond_wait(&cond, &mutex);

        DEBUG_LOG("THREAD_RUNABLE", "START");
        if (inputData.invalid) {
            printError("Input is too long");
        } else executeCommand();

        DEBUG_LOG("THREAD_RUNABLE", "END");
        threadStatus = READING;

        pthread_cond_signal(&cond);
        pthread_mutex_unlock(&mutex);
    }
}

void executeCommand() {
    if (strcmp(inputData.data, "exit") == 0) {
        DEBUG_LOG("EXECUTE", "EXIT COMMAND");
        programStatus = STOP;
    } else if (isEmptyCommand()) {
        DEBUG_LOG("EXECUTE", "EMPTY COMMAND");
        return;
    } else {
        DEBUG_LOG("EXECUTE", "COMMAND");
        struct parsed_input parsed;
        parseInput(&parsed);
        
        DEBUG_LOG("EXECUTE", parsed.program);
        DEBUG_LOG("EXECUTE", parsed.in);
        DEBUG_LOG("EXECUTE", parsed.out);
        
        if (inputData.invalid) {
            printError("Invalid command");
            return;
        }
        if (parsed.inLen != 0 && !checkRedirection(parsed.in, REDIRECT_IN))
        
        pid_t program = fork();
        if (!program) {
            if (!)
        } else if (program > 0) {
            struct sigaction sig;
            if (parsed.inBackgound) {
                initForkHandler(SIGCHLD, &sig, forkBackground);
            } else {
                initForkHandler(SIGINT, &sig, forkInteruptHandler);
                int status = 0;
                if (waitpid(program, &status, 0) < 0) {
                    printError("Waiting for program failed");
                }
            }
        } else perror("fork fail");
    }

}

void parseInput(struct parsed_input *parsed) {
    bool in = false, out = false, program = false;
    memset(parsed->in, 0, BUFFER_SIZE);
    memset(parsed->out, 0, BUFFER_SIZE);
    memset(parsed->program, 0, BUFFER_SIZE);
    parsed->inLen = parsed->outLen = parsed->length = 0;
    int state = 0;
    for (int i = 0; i < inputData.lengt; i++) {
        //fprintf(stdout, "%d\n", state);
        switch ((int) inputData.data[i]) {
            case REDIRECT_IN:
                if (state == 3 || in)
                    inputData.invalid =true;
                else state = 1;
                break;
            case REDIRECT_OUT:
                if (state == 3 || out)
                    inputData.invalid =true;
                else state = 2;
                break;
            case RUN_ON_BACKGROUND:
                if (state == 3)
                    inputData.invalid =true;
                else state = 3;
            default:
                break;
        }
        switch (state) {
            case 0:
                parsed->program[parsed->length] = inputData.data[i];
                parsed->length++;
                break;
            case 1:
                if (parsed->inLen == 0)continue;
                parsed->in[parsed->inLen] = inputData.data[i];
                parsed->inLen++;
                in = true;
                break;
            case 2:
                if (parsed->outLen == 0)continue;
                parsed->out[parsed->outLen] = inputData.data[i];
                parsed->outLen++;
                out = true;
                break;
            case 3:
                parsed->inBackgound = true;
                break;
            default:
                switch ((int) inputData.data[i]) {
                    case ' ':
                    case '\n':
                    case '\r':
                    case '\0':
                        continue;
                    default:
                        inputData.invalid = true;
                        break;
                }
                break;
        }
    }
}



bool isRunOnBackground() {
    bool onBackround = false;
    bool existsChars = false;
    unsigned int pos = 0;
    for (int i = inputData.lengt; i >= 0; i--) {
        if (isspace(inputData.data[i]) == true)
            existsChars = true;
        if (inputData.data[i] == RUN_ON_BACKGROUND) {
            if (onBackround) {
                printError("Duplicated &");
                return false;
            } else if (existsChars) {
                printError("Unexpected end of command");
                return false;
            }
            onBackround = true;
            pos = i;
        }
    }
    if (onBackround)
        inputData.data[pos] = '\0';
    return onBackround;
}

char *getIORedirect(char type) {
    char *founded = strstr(inputData.data, &type);

    if (founded == NULL)
        return "";
    DEBUG_LOG("IO_REDIRECT", founded);
}